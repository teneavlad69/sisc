const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");





//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament2"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50) UNIQUE,facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER,varsta VARCHAR(3),cnp VARCHAR(20),sex VARCHAR(10))";
    connection.query(sql, function (err, result) {
        if (err) {throw err};
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta = req.body.varsta;
    let cnp = req.body.cnp;
    let sex ;
    let error = [];

        if(!nume)
        {
            console.log("Ati uitat sa completati numele!");
            error.push("Ati uitat sa completati numele");
        }
        if(nume.length < 3 || nume.length > 20)
        {
            console.log("Nume invalid,sorry man!");
            error.push("Nume invalid,sorry man!");
        }
        else if(!nume.match("^[A-Za-z]+$")) 
        {
            console.log("Numele trebuie sa contina doar litere!");
            error.push("Numele trebuie sa contina doar litere!");
        }
    
        if(!prenume)
        {
            console.log("Ati uitat sa completati numele!");
            error.push("Ati uitat sa completati numele");
        }
        if(prenume.length < 3 || prenume.length > 20)
        {
            console.log("Prenumele invalid,sorry man!");
            error.push("Prenumele invalid,sorry man!");
        }
        else if(!prenume.match("^[A-Za-z]+$")) 
        {
            console.log("Prenumele trebuie sa contina doar litere!");
            error.push("Prenumele trebuie sa contina doar litere!");
        }

        if(!telefon)
        {
            console.log("Ati uitat sa completati cu nr dvs de telefon!");
            error.push("Ati uitat sa completati cu nr dcs de telefon");
        }
        if (telefon.length != 10)
         {
            console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
            error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
         }
        else if (!telefon.match("^[0-9]+$")) 
        {
            console.log("Numarul de telefon trebuie sa contina doar cifre!");
            error.push("Numarul de telefon trebuie sa contina doar cifre!");
        }
        if(!email)
        {
            console.log("Ati uitat sa completati tabela de email!");
            error.push("Ati uitat sa completati tabela de email");
        }
        if (!email.includes("@gmail.com") && !email.includes("@yahoo.com")) 
        {
            console.log("Email invalid!");
            error.push("Email invalid!");
        }

        if(!facebook)
        {
            console.log("Completati tabela de facebook!");
            error.push("Completati tabela de facebook");
        }
        if (!facebook.includes("www.facebook.com/") ) 
        {
            console.log("Facebook invalid!");
            error.push("Facebook invalid!");
        }
          
        if(!tipAbonament)
        {
            console.log("Ati uitat sa mentionati tipul de abonament!");
            error.push("Ati uitat sa mentionati tipul de abonament");
        }
        if(tipAbonament.length < 3 || tipAbonament.length > 20)
        {
            console.log("Tip de abonament invalid!");
            error.push("Tip de abonament invalid!");
        }
        else if(!tipAbonament.match("^[A-Za-z]+$")) 
        {
            console.log("Tipul de abonament trebuie sa contina doar litere!");
            error.push("Tipul de abonament trebuie  sa contina doar litere!");
        }
        
        if(!nrCard)
        {
            console.log("Ati uitat sa completati numarul de card");
            error.push("Ati uitat sa completati numarul de card");
        }
        if (nrCard.length != 16) 
        {
            console.log("Numarul de card trebuie sa fie de 16 cifre!");
            error.push("Numarul de card trebuie sa fie de 16 cifre!");
        } else if (!nrCard.match("^[0-9]+$")) 
        {
            console.log("Numarul de card trebuie sa contina doar cifre!");
            error.push("Numarul de card trebuie sa contina doar cifre!");
        }
        if(!cvv){
            console.log("Ati uitat sa completati in tabela cvv!");
            error.push("Ati uitat sa completati in tabela cvv");
        }
        if (cvv.length != 3) 
          {
            console.log("Numarul de cvv trebuie sa fie de 3 cifre!");
            error.push("Numarul de telefon trebuie sa fie de 3 cifre!");
          } 
        else if (!cvv.match("^[0-9]+$")) 
          {
            console.log("Numarul de cvv trebuie sa contina doar cifre!");
            error.push("Numarul de cvv trebuie sa contina doar cifre!");
          }

        if (varsta.length > 3) 
          {
            console.log("Varsta dvs trebuie sa fie de maxim 3 cifre!");
            error.push("Varsta dvs trebuie sa fie de maxim 3 cifre!");
          } 
        else if (!varsta.match("^[0-9]+$")) 
          {
            console.log("Varsta trebuie sa contina doar cifre!");
            error.push("Varsta trebuie sa contina doar cifre!");
          }

       //Varsta sa corespunda cu cnp-ul.
       //Pentru cei care sunt nascuti inainte de 2000
        if(cnp.substring(0,1)==1||cnp.substring(0,1)==2)
         {
            var v = cnp.substring(1, 3);
            var an = 19+v;
            var v_an= 2019-an;
          if(v_an!=varsta)
            {
              console.log("ATENTIE! cnp-ul nu corespunde cu varsta!");
              error.push("ATENTIE! cnp-ul nu corespunde cu varsta");
            }
          }
        //Pentru cei care sunt nascuti dupa 2000 
        if(cnp.substring(0,1)==3||cnp.substring(0,1)==4||cnp.substring(0,1)==5||cnp.substring(0,1)==6)
        {
          var v2 = cnp.substring(1,3);
          var an2=19-v2;
          console.log(an2);
          if(an2!=varsta)
            {
              console.log("ATENTIE! cnp-ul nu corespunde cu varsta!");
              error.push("ATENTIE! cnp-ul nu corespunde cu varsta");
            }
        }
        

          if (cnp.length != 13) 
          {
            console.log("Numarul de cnp trebuie sa fie de 13 cifre!");
            error.push("Numarul de cnp trebuie sa fie de 13 cifre!");
          } 
          else if (!cnp.match("^[0-9]+$")) 
          {
            console.log("Numarul de cnp trebuie sa contina doar cifre!");
            error.push("Numarul de cnp trebuie sa contina doar cifre!");
          }
          //Adaugare sex in functie de CNP
          if(cnp.substring(0 ,1)==1 || cnp.substring(0 ,1)== 3 || cnp.substring(0 ,1)== 5 )
        {
            sex = "M";
        }
          if(cnp.substring(0 ,1)==2||cnp.substring(0 ,1)==4||cnp.substring(0 ,1)==6)
          {
              sex = "F";
          }


       if (error.length === 0) 
       {
        const sql =
            "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv,varsta,cnp,sex) VALUES('" +nume +"','" +prenume +"','" +telefon +"','" +email +"','" +facebook +"','" + tipAbonament +"','" + nrCard +"','" +cvv+"','" + varsta +"','" + cnp +"','" + sex +"')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
       } 
           else  
           {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    //Unicitate email.
        res.status(400).send({
          message: "Emial-ul este deja utilizat"});
        console.log("Email-ul exista deja");
           }
             });